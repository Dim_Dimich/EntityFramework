﻿using EntityFramework.Abstration;
using EntityFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    class UnitOfWork : IDisposable
    {
        private readonly UniversityDbContext _context = new UniversityDbContext();
        private StudentModelRepository _studentModelRepository;
        private GroupModelRepository _groupModelRepository;
        private FacultyModelRepository _facultyModelRepository;

        //public UnitOfWork(IStudentModelRepository studentModelRepository, IGroupModelRepository groupModelRepository, IFacultyModelRepository facultyModelRepository)
        //{
        //    this._studentModelRepository = studentModelRepository;
        //    this._groupModelRepository = groupModelRepository;
        //    this._facultyModelRepository = facultyModelRepository;
        //}

        //public UnitOfWork()
        //{

        //}

        public StudentModelRepository StudentModelRepository
        {
            get
            {
                if (this._studentModelRepository == null)
                {
                    this._studentModelRepository = new StudentModelRepository(_context);
                }
                return this._studentModelRepository;
            }
        }

        public GroupModelRepository GroupModelRepository
        {
            get
            {
                if (this._groupModelRepository == null)
                {
                    this._groupModelRepository = new GroupModelRepository(_context);
                }
                return this._groupModelRepository;
            }
        }

        public FacultyModelRepository FacultyModelRepository
        {
            get
            {
                if (this._facultyModelRepository == null)
                {
                    this._facultyModelRepository = new FacultyModelRepository(_context);
                }
                return this._facultyModelRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
