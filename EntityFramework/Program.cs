﻿using EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            FirstFunction();
            Console.WriteLine("Done");
            Console.ReadKey();
        }

        public static void FirstFunction()
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var student = new Student()
                {
                    FirstName = "Dmytro",
                    LastName = "Rakochyi",
                    Age = 18,
                    Id = 1
                };
                    unitOfWork.StudentModelRepository.CreateStudent(student);
                    unitOfWork.Save();
            }
        }
    }
}
