﻿using EntityFramework.Abstration;
using EntityFramework.Repositories;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework
{
    class UniversityNinjectModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IStudentModelRepository>().To<StudentModelRepository>();
            this.Bind<IGroupModelRepository>().To<GroupModelRepository>();
            this.Bind<IFacultyModelRepository>().To<FacultyModelRepository>();
        }
    }
}
