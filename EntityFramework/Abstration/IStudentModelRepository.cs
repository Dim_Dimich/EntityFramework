﻿using EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Abstration
{
    interface IStudentModelRepository : IDisposable
    {
        IQueryable<Student> GetAllStudents();
        Student GetStudentById(int studentId);
        void CreateStudent(Student student);
        void DeleteStudent(int studentId);
        void ChangeStudent(Student student);
        void Save();
    }
}
