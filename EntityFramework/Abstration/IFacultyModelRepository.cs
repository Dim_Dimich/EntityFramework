﻿using EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Abstration
{
    interface IFacultyModelRepository : IDisposable
    {
        IQueryable<Faculty> GetAllFaculties();
        Faculty GetFacultyById(int facultyId);
        void CreateFaculty(Faculty faculty);
        void DeleteFaculty(int facultyId);
        void ChangeFaculty(Faculty faculty);
        void Save();
    }
}
