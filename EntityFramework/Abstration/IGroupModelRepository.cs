﻿using EntityFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework.Abstration
{
    interface IGroupModelRepository : IDisposable
    {
        IQueryable<Group> GetAllGroups();
        Group GetGroupById(int groupId);
        void CreateGroup(Group group);
        void DeleteGroup(int groupId);
        void ChangeGroup(Group group);
        void Save();
    }
}
