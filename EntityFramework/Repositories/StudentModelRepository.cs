﻿using EntityFramework.Abstration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Models;

namespace EntityFramework.Repositories
{
    class StudentModelRepository : IStudentModelRepository
    {
        private readonly UniversityDbContext _context;

        public StudentModelRepository(UniversityDbContext context)
        {
            this._context = context;
        }

        public void ChangeStudent(Student student)
        {
            _context.Entry(student).State = EntityState.Modified;
        }

        public void CreateStudent(Student student)
        {
            _context.Students.Add(student);
        }

        public void DeleteStudent(int studentId)
        {
            var student = _context.Students.Find(studentId);
            _context.Students.Remove(student);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Student> GetAllStudents()
        {
            return _context.Students;
        }

        public Student GetStudentById(int studentId)
        {
            return _context.Students.Find(studentId);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
