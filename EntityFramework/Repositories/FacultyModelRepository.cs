﻿using EntityFramework.Abstration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Models;

namespace EntityFramework.Repositories
{
    class FacultyModelRepository : IFacultyModelRepository
    {
        private readonly UniversityDbContext _context;

        public FacultyModelRepository(UniversityDbContext context)
        {
            this._context = context;
        }

        public void ChangeFaculty(Faculty faculty)
        {
            _context.Entry(faculty).State = EntityState.Modified;
        }

        public void CreateFaculty(Faculty faculty)
        {
            _context.Faculties.Add(faculty);
        }

        public void DeleteFaculty(int facultyId)
        {
            var faculty = _context.Faculties.Find(facultyId);
            _context.Faculties.Remove(faculty);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Faculty> GetAllFaculties()
        {
            return _context.Faculties;
        }

        public Faculty GetFacultyById(int facultyId)
        {
            return _context.Faculties.Find(facultyId);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
