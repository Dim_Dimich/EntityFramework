﻿using EntityFramework.Abstration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.Models;

namespace EntityFramework.Repositories
{
    class GroupModelRepository : IGroupModelRepository
    {
        private readonly UniversityDbContext _context;

        public GroupModelRepository(UniversityDbContext context)
        {
            this._context = context;
        }

        public void ChangeGroup(Group group)
        {
            _context.Entry(group).State = EntityState.Modified;
        }

        public void CreateGroup(Group group)
        {
            _context.Groups.Add(group);
        }

        public void DeleteGroup(int groupId)
        {
            var group = _context.Groups.Find(groupId);
            _context.Groups.Remove(group);
        }

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IQueryable<Group> GetAllGroups()
        {
            return _context.Groups;
        }

        public Group GetGroupById(int groupId)
        {
            return _context.Groups.Find(groupId);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
